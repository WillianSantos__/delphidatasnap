unit ServerMethodsUnit1;

interface

uses
   System.SysUtils,
   System.Classes,
   System.Json,
   System.Variants,
   DataSnap.DSProviderDataModuleAdapter,
   DataSnap.DSServer,
   DataSnap.DSAuth,
   FireDAC.Phys.FBDef,
   FireDAC.UI.Intf,
   FireDAC.VCLUI.Wait,
   FireDAC.Stan.Intf,
   FireDAC.Stan.Option,
   FireDAC.Stan.Error,
   FireDAC.Phys.Intf,
   FireDAC.Stan.Def,
   FireDAC.Stan.Pool,
   FireDAC.Stan.Async,
   FireDAC.Phys,
   FireDAC.Phys.FB,
   Data.DB,
   FireDAC.Comp.Client,
   FireDAC.Comp.UI,
   FireDAC.Phys.IBBase,
   FireDAC.Stan.Param,
   FireDAC.DatS,
   FireDAC.DApt.Intf,
   FireDAC.DApt,
   FireDAC.Comp.DataSet,
   Data.FireDACJSONReflect,
   FireDAC.Stan.StorageJSON,
   FireDAC.Stan.StorageXML,
   FireDAC.Stan.StorageBin;

type
   TServerMethods1 = class(TDSServerModule)
      FDPhysFBDriverLink1: TFDPhysFBDriverLink;
      FDGUIxWaitCursor1: TFDGUIxWaitCursor;
      FDConnection1: TFDConnection;
      FDQuery1: TFDQuery;
      FDStanStorageJSONLink1: TFDStanStorageJSONLink;
      FDStanStorageBinLink1: TFDStanStorageBinLink;
      FDStanStorageXMLLink1: TFDStanStorageXMLLink;
   private
      { Private declarations }
   public
      { Public declarations }
      function EchoString(Value: string): string;
      function ReverseString(Value: string): string;

      function Sum(A, B: integer): integer;

      function EmployeeListJSON: TJSONObject;

      function EmployeeList: TFDJSONDataSets;
      procedure EmployeeApplyUpdates(ADeltaList: TFDJSONDeltas);
   end;

const
   cEmployee = 'Employee';

implementation

{$R *.dfm}

uses System.StrUtils;

function GetDataSetAsJSON(ADataSet: TDataSet): TJSONObject;
var
   LField: TField;
   LJSONObject: TJSONObject;
   LJSONArray: TJSONArray;
begin
   LJSONArray := TJSONArray.Create;
   ADataSet.Open;

   ADataSet.First;
   while not ADataSet.EOF do
   begin
      LJSONObject := TJSONObject.Create;
      for LField in ADataSet.Fields do
         LJSONObject.AddPair(LField.FieldName, VarToStr(LField.Value));
      LJSONArray.AddElement(LJSONObject);
      ADataSet.Next;
   end;
   ADataSet.Close;

   Result := TJSONObject.Create;
   Result.AddPair(ADataSet.Name, LJSONArray);
end;

function TServerMethods1.EchoString(Value: string): string;
begin
   Result := Value;
end;

function TServerMethods1.ReverseString(Value: string): string;
begin
   Result := System.StrUtils.ReverseString(Value);
end;

function TServerMethods1.Sum(A, B: integer): integer;
begin
   Result := A + B;
end;

function TServerMethods1.EmployeeListJSON: TJSONObject;
begin
   Result := GetDataSetAsJSON(FDQuery1);
end;

function TServerMethods1.EmployeeList: TFDJSONDataSets;
begin
   FDQuery1.Close;
   Result := TFDJSONDataSets.Create;
   TFDJSONDataSetsWriter.ListAdd(Result, cEmployee, FDQuery1);
end;

procedure TServerMethods1.EmployeeApplyUpdates(ADeltaList: TFDJSONDeltas);
var
   LApply: IFDJSONDeltasApplyUpdates;
begin
   LApply := TFDJSONDeltasApplyUpdates.Create(ADeltaList);
   LApply.ApplyUpdates(cEmployee, FDQuery1.Command);

   if LApply.Errors.Count = 0 then
      LApply.ApplyUpdates(cEmployee, FDQuery1.Command);

   if (LApply.Errors.Count > 0) then
      raise Exception.Create(LApply.Errors.Strings.Text);
end;

end.
