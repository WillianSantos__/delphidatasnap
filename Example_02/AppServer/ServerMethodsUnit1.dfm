object ServerMethods1: TServerMethods1
  OldCreateOrder = False
  Height = 461
  Width = 592
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 80
    Top = 16
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 80
    Top = 64
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      
        'Database=C:\Program Files (x86)\Firebird\Firebird_2_5\examples\e' +
        'mpbuild\EMPLOYEE.FDB'
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    LoginPrompt = False
    Left = 80
    Top = 112
  end
  object FDQuery1: TFDQuery
    CachedUpdates = True
    Connection = FDConnection1
    SQL.Strings = (
      'select * from employee')
    Left = 80
    Top = 160
  end
  object FDStanStorageJSONLink1: TFDStanStorageJSONLink
    Left = 208
    Top = 16
  end
  object FDStanStorageBinLink1: TFDStanStorageBinLink
    Left = 208
    Top = 64
  end
  object FDStanStorageXMLLink1: TFDStanStorageXMLLink
    Left = 208
    Top = 112
  end
end
