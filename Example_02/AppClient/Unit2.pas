unit Unit2;

interface

uses
   Winapi.Windows,
   Winapi.Messages,
   System.SysUtils,
   System.Variants,
   System.Classes,
   Vcl.Graphics,
   Vcl.Controls,
   Vcl.Forms,
   Vcl.Dialogs,
   Vcl.Grids,
   Data.DB,
   Data.Bind.Components,
   Data.Bind.DBScope,
   Vcl.StdCtrls,
   Vcl.ExtCtrls,
   Vcl.DBCtrls,
   Vcl.DBGrids;

type
   TForm2 = class(TForm)
      DataSource1: TDataSource;
      DBGrid1: TDBGrid;
      DBNavigator1: TDBNavigator;
      Button1: TButton;
      Button2: TButton;
      procedure Button1Click(Sender: TObject);
      procedure Button2Click(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form2: TForm2;

implementation

uses
   ClientModuleUnit1;

{$R *.dfm}

procedure TForm2.Button1Click(Sender: TObject);
begin
   ClientModule1.EmployeeList;
end;

procedure TForm2.Button2Click(Sender: TObject);
begin
   ClientModule1.EmployeeApplyUpdates;
end;

end.
