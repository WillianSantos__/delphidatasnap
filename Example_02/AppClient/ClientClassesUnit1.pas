//
// Created by the DataSnap proxy generator.
// 17/03/2018 17:17:08
//

unit ClientClassesUnit1;

interface

uses System.JSON, Datasnap.DSProxyRest, Datasnap.DSClientRest, Data.DBXCommon, Data.DBXClient, Data.DBXDataSnap, Data.DBXJSON, Datasnap.DSProxy, System.Classes, System.SysUtils, Data.DB, Data.SqlExpr, Data.DBXDBReaders, Data.DBXCDSReaders, Data.FireDACJSONReflect, Data.DBXJSONReflect;

type

  IDSRestCachedTFDJSONDataSets = interface;

  TServerMethods1Client = class(TDSAdminRestClient)
  private
    FEchoStringCommand: TDSRestCommand;
    FReverseStringCommand: TDSRestCommand;
    FSumCommand: TDSRestCommand;
    FEmployeeListJSONCommand: TDSRestCommand;
    FEmployeeListJSONCommand_Cache: TDSRestCommand;
    FEmployeeListCommand: TDSRestCommand;
    FEmployeeListCommand_Cache: TDSRestCommand;
    FEmployeeApplyUpdatesCommand: TDSRestCommand;
  public
    constructor Create(ARestConnection: TDSRestConnection); overload;
    constructor Create(ARestConnection: TDSRestConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function EchoString(Value: string; const ARequestFilter: string = ''): string;
    function ReverseString(Value: string; const ARequestFilter: string = ''): string;
    function Sum(A: Integer; B: Integer; const ARequestFilter: string = ''): Integer;
    function EmployeeListJSON(const ARequestFilter: string = ''): TJSONObject;
    function EmployeeListJSON_Cache(const ARequestFilter: string = ''): IDSRestCachedJSONObject;
    function EmployeeList(const ARequestFilter: string = ''): TFDJSONDataSets;
    function EmployeeList_Cache(const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    procedure EmployeeApplyUpdates(ADeltaList: TFDJSONDeltas);
  end;

  IDSRestCachedTFDJSONDataSets = interface(IDSRestCachedObject<TFDJSONDataSets>)
  end;

  TDSRestCachedTFDJSONDataSets = class(TDSRestCachedObject<TFDJSONDataSets>, IDSRestCachedTFDJSONDataSets, IDSRestCachedCommand)
  end;

const
  TServerMethods1_EchoString: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Value'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

  TServerMethods1_ReverseString: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Value'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

  TServerMethods1_Sum: array [0..2] of TDSRestParameterMetaData =
  (
    (Name: 'A'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'B'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 6; TypeName: 'Integer')
  );

  TServerMethods1_EmployeeListJSON: array [0..0] of TDSRestParameterMetaData =
  (
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TJSONObject')
  );

  TServerMethods1_EmployeeListJSON_Cache: array [0..0] of TDSRestParameterMetaData =
  (
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethods1_EmployeeList: array [0..0] of TDSRestParameterMetaData =
  (
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TServerMethods1_EmployeeList_Cache: array [0..0] of TDSRestParameterMetaData =
  (
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethods1_EmployeeApplyUpdates: array [0..0] of TDSRestParameterMetaData =
  (
    (Name: 'ADeltaList'; Direction: 1; DBXType: 37; TypeName: 'TFDJSONDeltas')
  );

implementation

function TServerMethods1Client.EchoString(Value: string; const ARequestFilter: string): string;
begin
  if FEchoStringCommand = nil then
  begin
    FEchoStringCommand := FConnection.CreateCommand;
    FEchoStringCommand.RequestType := 'GET';
    FEchoStringCommand.Text := 'TServerMethods1.EchoString';
    FEchoStringCommand.Prepare(TServerMethods1_EchoString);
  end;
  FEchoStringCommand.Parameters[0].Value.SetWideString(Value);
  FEchoStringCommand.Execute(ARequestFilter);
  Result := FEchoStringCommand.Parameters[1].Value.GetWideString;
end;

function TServerMethods1Client.ReverseString(Value: string; const ARequestFilter: string): string;
begin
  if FReverseStringCommand = nil then
  begin
    FReverseStringCommand := FConnection.CreateCommand;
    FReverseStringCommand.RequestType := 'GET';
    FReverseStringCommand.Text := 'TServerMethods1.ReverseString';
    FReverseStringCommand.Prepare(TServerMethods1_ReverseString);
  end;
  FReverseStringCommand.Parameters[0].Value.SetWideString(Value);
  FReverseStringCommand.Execute(ARequestFilter);
  Result := FReverseStringCommand.Parameters[1].Value.GetWideString;
end;

function TServerMethods1Client.Sum(A: Integer; B: Integer; const ARequestFilter: string): Integer;
begin
  if FSumCommand = nil then
  begin
    FSumCommand := FConnection.CreateCommand;
    FSumCommand.RequestType := 'GET';
    FSumCommand.Text := 'TServerMethods1.Sum';
    FSumCommand.Prepare(TServerMethods1_Sum);
  end;
  FSumCommand.Parameters[0].Value.SetInt32(A);
  FSumCommand.Parameters[1].Value.SetInt32(B);
  FSumCommand.Execute(ARequestFilter);
  Result := FSumCommand.Parameters[2].Value.GetInt32;
end;

function TServerMethods1Client.EmployeeListJSON(const ARequestFilter: string): TJSONObject;
begin
  if FEmployeeListJSONCommand = nil then
  begin
    FEmployeeListJSONCommand := FConnection.CreateCommand;
    FEmployeeListJSONCommand.RequestType := 'GET';
    FEmployeeListJSONCommand.Text := 'TServerMethods1.EmployeeListJSON';
    FEmployeeListJSONCommand.Prepare(TServerMethods1_EmployeeListJSON);
  end;
  FEmployeeListJSONCommand.Execute(ARequestFilter);
  Result := TJSONObject(FEmployeeListJSONCommand.Parameters[0].Value.GetJSONValue(FInstanceOwner));
end;

function TServerMethods1Client.EmployeeListJSON_Cache(const ARequestFilter: string): IDSRestCachedJSONObject;
begin
  if FEmployeeListJSONCommand_Cache = nil then
  begin
    FEmployeeListJSONCommand_Cache := FConnection.CreateCommand;
    FEmployeeListJSONCommand_Cache.RequestType := 'GET';
    FEmployeeListJSONCommand_Cache.Text := 'TServerMethods1.EmployeeListJSON';
    FEmployeeListJSONCommand_Cache.Prepare(TServerMethods1_EmployeeListJSON_Cache);
  end;
  FEmployeeListJSONCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedJSONObject.Create(FEmployeeListJSONCommand_Cache.Parameters[0].Value.GetString);
end;

function TServerMethods1Client.EmployeeList(const ARequestFilter: string): TFDJSONDataSets;
begin
  if FEmployeeListCommand = nil then
  begin
    FEmployeeListCommand := FConnection.CreateCommand;
    FEmployeeListCommand.RequestType := 'GET';
    FEmployeeListCommand.Text := 'TServerMethods1.EmployeeList';
    FEmployeeListCommand.Prepare(TServerMethods1_EmployeeList);
  end;
  FEmployeeListCommand.Execute(ARequestFilter);
  if not FEmployeeListCommand.Parameters[0].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FEmployeeListCommand.Parameters[0].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FEmployeeListCommand.Parameters[0].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FEmployeeListCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TServerMethods1Client.EmployeeList_Cache(const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FEmployeeListCommand_Cache = nil then
  begin
    FEmployeeListCommand_Cache := FConnection.CreateCommand;
    FEmployeeListCommand_Cache.RequestType := 'GET';
    FEmployeeListCommand_Cache.Text := 'TServerMethods1.EmployeeList';
    FEmployeeListCommand_Cache.Prepare(TServerMethods1_EmployeeList_Cache);
  end;
  FEmployeeListCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FEmployeeListCommand_Cache.Parameters[0].Value.GetString);
end;

procedure TServerMethods1Client.EmployeeApplyUpdates(ADeltaList: TFDJSONDeltas);
begin
  if FEmployeeApplyUpdatesCommand = nil then
  begin
    FEmployeeApplyUpdatesCommand := FConnection.CreateCommand;
    FEmployeeApplyUpdatesCommand.RequestType := 'POST';
    FEmployeeApplyUpdatesCommand.Text := 'TServerMethods1."EmployeeApplyUpdates"';
    FEmployeeApplyUpdatesCommand.Prepare(TServerMethods1_EmployeeApplyUpdates);
  end;
  if not Assigned(ADeltaList) then
    FEmployeeApplyUpdatesCommand.Parameters[0].Value.SetNull
  else
  begin
    FMarshal := TDSRestCommand(FEmployeeApplyUpdatesCommand.Parameters[0].ConnectionHandler).GetJSONMarshaler;
    try
      FEmployeeApplyUpdatesCommand.Parameters[0].Value.SetJSONValue(FMarshal.Marshal(ADeltaList), True);
      if FInstanceOwner then
        ADeltaList.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FEmployeeApplyUpdatesCommand.Execute;
end;

constructor TServerMethods1Client.Create(ARestConnection: TDSRestConnection);
begin
  inherited Create(ARestConnection);
end;

constructor TServerMethods1Client.Create(ARestConnection: TDSRestConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ARestConnection, AInstanceOwner);
end;

destructor TServerMethods1Client.Destroy;
begin
  FEchoStringCommand.DisposeOf;
  FReverseStringCommand.DisposeOf;
  FSumCommand.DisposeOf;
  FEmployeeListJSONCommand.DisposeOf;
  FEmployeeListJSONCommand_Cache.DisposeOf;
  FEmployeeListCommand.DisposeOf;
  FEmployeeListCommand_Cache.DisposeOf;
  FEmployeeApplyUpdatesCommand.DisposeOf;
  inherited;
end;

end.

