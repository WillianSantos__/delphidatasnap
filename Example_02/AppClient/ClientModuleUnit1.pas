unit ClientModuleUnit1;

interface

uses
   System.SysUtils,
   System.Classes,
   ClientClassesUnit1,
   Datasnap.DSClientRest,
   FireDAC.Stan.StorageXML,
   FireDAC.Stan.StorageJSON,
   FireDAC.Stan.StorageBin,
   FireDAC.Stan.Intf,
   FireDAC.Stan.Option,
   FireDAC.Stan.Param,
   FireDAC.Stan.Error,
   FireDAC.DatS,
   FireDAC.Phys.Intf,
   FireDAC.DApt.Intf,
   Data.DB,
   FireDAC.Comp.DataSet,
   FireDAC.Comp.Client,
   Data.FireDACJSONReflect;

type
   TClientModule1 = class(TDataModule)
      DSRestConnection1: TDSRestConnection;
      FDStanStorageBinLink1: TFDStanStorageBinLink;
      FDStanStorageJSONLink1: TFDStanStorageJSONLink;
      FDStanStorageXMLLink1: TFDStanStorageXMLLink;
      FDMemTable1: TFDMemTable;
   private
      FInstanceOwner: Boolean;
      FServerMethods1Client: TServerMethods1Client;
      function GetServerMethods1Client: TServerMethods1Client;
      { Private declarations }
   public
      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;

      procedure EmployeeList;
      function EmployeeDelta: TFDJsonDeltas;
      procedure EmployeeApplyUpdates;

      property InstanceOwner: Boolean read FInstanceOwner write FInstanceOwner;
      property ServerMethods1Client: TServerMethods1Client read GetServerMethods1Client write FServerMethods1Client;
   end;

var
   ClientModule1: TClientModule1;

implementation

{$R *.dfm}

procedure TClientModule1.EmployeeList;
var
   LDataSetList: TFDJSONDataSets;
   LDataSet: TFDDataSet;
begin
   LDataSetList := Self.ServerMethods1Client.EmployeeList;
   LDataSet := TFDJSONDataSetsReader.GetListValueByName( //
      LDataSetList, 'Employee');

   FDMemTable1.Close;
   FDMemTable1.AppendData(LDataSet);
end;

function TClientModule1.EmployeeDelta: TFDJsonDeltas;
begin
   if FDMemTable1.State in dsEditModes then
      FDMemTable1.Post;

   Result := TFDJsonDeltas.Create;
   TFDJSONDeltasWriter.ListAdd(Result, 'Employee', FDMemTable1);
end;

procedure TClientModule1.EmployeeApplyUpdates;
var
   LDeltaList: TFDJsonDeltas;
begin
   LDeltaList := EmployeeDelta;
   Self.ServerMethods1Client.EmployeeApplyUpdates(LDeltaList);
end;

constructor TClientModule1.Create(AOwner: TComponent);
begin
   inherited;
   FInstanceOwner := True;
end;

destructor TClientModule1.Destroy;
begin
   FServerMethods1Client.Free;
   inherited;
end;

function TClientModule1.GetServerMethods1Client: TServerMethods1Client;
begin
   if FServerMethods1Client = nil then
      FServerMethods1Client := TServerMethods1Client.Create(DSRestConnection1, FInstanceOwner);
   Result := FServerMethods1Client;
end;

end.
