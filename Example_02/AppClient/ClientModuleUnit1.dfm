object ClientModule1: TClientModule1
  OldCreateOrder = False
  Height = 271
  Width = 415
  object DSRestConnection1: TDSRestConnection
    Host = 'localhost'
    Port = 8080
    LoginPrompt = False
    Left = 80
    Top = 24
    UniqueId = '{F968F3EE-A004-4129-9E7C-86FDADF40CB0}'
  end
  object FDStanStorageBinLink1: TFDStanStorageBinLink
    Left = 80
    Top = 72
  end
  object FDStanStorageJSONLink1: TFDStanStorageJSONLink
    Left = 80
    Top = 120
  end
  object FDStanStorageXMLLink1: TFDStanStorageXMLLink
    Left = 80
    Top = 168
  end
  object FDMemTable1: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 264
    Top = 24
  end
end
