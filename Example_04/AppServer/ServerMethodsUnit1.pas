unit ServerMethodsUnit1;

interface

uses
   System.SysUtils,
   System.Classes,
   System.Json,
   DataSnap.DSProviderDataModuleAdapter,
   DataSnap.DSServer,
   DataSnap.DSAuth,
   FireDAC.Phys.FBDef,
   FireDAC.UI.Intf,
   FireDAC.VCLUI.Wait,
   FireDAC.Stan.Intf,
   FireDAC.Stan.Option,
   FireDAC.Stan.Error,
   FireDAC.Phys.Intf,
   FireDAC.Stan.Def,
   FireDAC.Stan.Pool,
   FireDAC.Stan.Async,
   FireDAC.Phys,
   FireDAC.Stan.Param,
   FireDAC.DatS,
   FireDAC.DApt.Intf,
   FireDAC.DApt,
   Data.DB,
   FireDAC.Comp.DataSet,
   FireDAC.Comp.Client,
   FireDAC.Comp.UI,
   FireDAC.Phys.IBBase,
   FireDAC.Phys.FB,
   Data.DBXFirebird,
   Data.SqlExpr,
   Data.FMTBcd,
   DataSnap.Provider;

type
   TServerMethods1 = class(TDSServerModule)
      FDPhysFBDriverLink: TFDPhysFBDriverLink;
      FDGUIxWaitCursor: TFDGUIxWaitCursor;
      FDConn: TFDConnection;
      FDQryEmployee: TFDQuery;
      SQLConn: TSQLConnection;
      SQLQryEmployee: TSQLQuery;
      dspFDQryEmployee: TDataSetProvider;
      dspSQLQryEmployee: TDataSetProvider;
      FDQryCustomer: TFDQuery;
      dspFDQryCustomer: TDataSetProvider;
    SQLQryCustomer: TSQLQuery;
    dspSQLQryCustomer: TDataSetProvider;
      procedure FDQryEmployeeCommandChanged(Sender: TObject);
      procedure FDQryEmployeeFilterRecord(DataSet: TDataSet; var Accept: Boolean);
   private
      procedure LogSystem(AText: string);
      { Private declarations }
   public
      { Public declarations }
      function EchoString(Value: string): string;
      function ReverseString(Value: string): string;
   end;

implementation

{$R *.dfm}

uses
   System.StrUtils;

procedure TServerMethods1.LogSystem(AText: string);
var
   SL: TStringList;
   strFileNow: string;
   strFile: string;

const
   cFile = 'Debug%s.log';

begin
   strFileNow := FormatDateTime('yyyymmdd', Now);
   strFile := ExtractFilePath(ParamStr(0)) + Format(cFile, [strFileNow]);

   SL := TStringList.Create;
   try
      if FileExists(strFile) then
         SL.LoadFromFile(strFile);

      SL.Add(FormatDateTime('hh:nn:ss.zzz', Now) + ': ' + AText);

   finally
      FreeAndNil(SL);
   end;
end;

function TServerMethods1.EchoString(Value: string): string;
begin
   Result := Value;
end;

function TServerMethods1.ReverseString(Value: string): string;
begin
   Result := System.StrUtils.ReverseString(Value);
end;

procedure TServerMethods1.FDQryEmployeeCommandChanged(Sender: TObject);
begin
   LogSystem('FDQryEmployeeCommandChanged');
end;

procedure TServerMethods1.FDQryEmployeeFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
   LogSystem('FDQryEmployeeFilterRecord');
end;

end.
