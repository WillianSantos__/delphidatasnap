unit Unit1;

interface

uses
   Winapi.Windows,
   Winapi.Messages,
   System.SysUtils,
   System.Variants,
   System.Classes,
   Vcl.Graphics,
   Vcl.Controls,
   Vcl.Forms,
   Vcl.Dialogs,
   IdBaseComponent,
   IdComponent,
   IdIPWatch,
   Vcl.StdCtrls;

type
   TForm1 = class(TForm)
      Label1: TLabel;
      IdIPWatch1: TIdIPWatch;
      procedure FormCreate(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
   Self.Caption := 'AppServer';

   IdIPWatch1.Active := False;
   IdIPWatch1.HistoryEnabled := False;
   IdIPWatch1.Active := True;

   Label1.Caption := Self.Caption + ' IP: ' + IdIPWatch1.LocalIP;
end;

end.
