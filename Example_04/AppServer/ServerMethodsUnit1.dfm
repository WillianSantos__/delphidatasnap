object ServerMethods1: TServerMethods1
  OldCreateOrder = False
  Height = 470
  Width = 585
  object FDPhysFBDriverLink: TFDPhysFBDriverLink
    Left = 56
    Top = 16
  end
  object FDGUIxWaitCursor: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 56
    Top = 64
  end
  object FDConn: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      
        'Database=C:\Program Files (x86)\Firebird\Firebird_2_5\examples\e' +
        'mpbuild\EMPLOYEE.FDB'
      'DriverID=FB')
    LoginPrompt = False
    Left = 56
    Top = 112
  end
  object FDQryEmployee: TFDQuery
    OnFilterRecord = FDQryEmployeeFilterRecord
    Connection = FDConn
    OnCommandChanged = FDQryEmployeeCommandChanged
    SQL.Strings = (
      'SELECT * FROM EMPLOYEE')
    Left = 56
    Top = 160
  end
  object SQLConn: TSQLConnection
    DriverName = 'Firebird'
    LoginPrompt = False
    Params.Strings = (
      'DriverUnit=Data.DBXFirebird'
      
        'DriverPackageLoader=TDBXDynalinkDriverLoader,DbxCommonDriver250.' +
        'bpl'
      
        'DriverAssemblyLoader=Borland.Data.TDBXDynalinkDriverLoader,Borla' +
        'nd.Data.DbxCommonDriver,Version=24.0.0.0,Culture=neutral,PublicK' +
        'eyToken=91d62ebb5b0d1b1b'
      
        'MetaDataPackageLoader=TDBXFirebirdMetaDataCommandFactory,DbxFire' +
        'birdDriver250.bpl'
      
        'MetaDataAssemblyLoader=Borland.Data.TDBXFirebirdMetaDataCommandF' +
        'actory,Borland.Data.DbxFirebirdDriver,Version=24.0.0.0,Culture=n' +
        'eutral,PublicKeyToken=91d62ebb5b0d1b1b'
      'GetDriverFunc=getSQLDriverINTERBASE'
      'LibraryName=dbxfb.dll'
      'LibraryNameOsx=libsqlfb.dylib'
      'VendorLib=fbclient.dll'
      'VendorLibWin64=fbclient.dll'
      'VendorLibOsx=/Library/Frameworks/Firebird.framework/Firebird'
      
        'Database=C:\Program Files (x86)\Firebird\Firebird_2_5\examples\e' +
        'mpbuild\EMPLOYEE.FDB'
      'User_Name=sysdba'
      'Password=masterkey'
      'Role=RoleName'
      'MaxBlobSize=-1'
      'LocaleCode=0000'
      'IsolationLevel=ReadCommitted'
      'SQLDialect=3'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'TrimChar=False'
      'BlobSize=-1'
      'ErrorResourceFile='
      'RoleName=RoleName'
      'ServerCharSet='
      'Trim Char=False')
    Left = 312
    Top = 16
  end
  object SQLQryEmployee: TSQLQuery
    MaxBlobSize = -1
    ParamCheck = False
    Params = <>
    SQL.Strings = (
      'SELECT * FROM EMPLOYEE')
    SQLConnection = SQLConn
    Left = 312
    Top = 64
  end
  object dspFDQryEmployee: TDataSetProvider
    DataSet = FDQryEmployee
    Options = [poAllowCommandText, poUseQuoteChar]
    Left = 56
    Top = 208
  end
  object dspSQLQryEmployee: TDataSetProvider
    DataSet = SQLQryEmployee
    Options = [poAllowCommandText, poUseQuoteChar]
    Left = 312
    Top = 112
  end
  object FDQryCustomer: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT * FROM CUSTOMER'
      'WHERE CUST_NO = :CUST_NO')
    Left = 56
    Top = 256
    ParamData = <
      item
        Name = 'CUST_NO'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object dspFDQryCustomer: TDataSetProvider
    DataSet = FDQryCustomer
    Options = [poAllowCommandText, poUseQuoteChar]
    Left = 48
    Top = 304
  end
  object SQLQryCustomer: TSQLQuery
    MaxBlobSize = -1
    ParamCheck = False
    Params = <>
    SQL.Strings = (
      'SELECT * FROM CUSTOMER')
    SQLConnection = SQLConn
    Left = 312
    Top = 168
  end
  object dspSQLQryCustomer: TDataSetProvider
    DataSet = SQLQryCustomer
    Exported = False
    Options = [poAllowCommandText, poUseQuoteChar]
    Left = 312
    Top = 216
  end
end
