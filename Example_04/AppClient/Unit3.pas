unit Unit3;

interface

uses
   Winapi.Windows,
   Winapi.Messages,
   System.SysUtils,
   System.Variants,
   System.Classes,
   Vcl.Graphics,
   Vcl.Controls,
   Vcl.Forms,
   Vcl.Dialogs,
   Data.DB,
   Vcl.Grids,
   Vcl.DBGrids,
   Vcl.ExtCtrls,
   Vcl.StdCtrls;

type
   TForm3 = class(TForm)
      Panel1: TPanel;
      DBGrid1: TDBGrid;
      DataSource1: TDataSource;
      btnFD: TButton;
      btnDBX: TButton;
      procedure FormCreate(Sender: TObject);
      procedure btnDBXClick(Sender: TObject);
      procedure btnFDClick(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form3: TForm3;

implementation

uses
   ClientModuleUnit1;

{$R *.dfm}

procedure TForm3.FormCreate(Sender: TObject);
begin
   Self.Caption := 'AppClient';
end;

procedure TForm3.btnDBXClick(Sender: TObject);
var
   t1, t2, t3: TTime;
begin
   t1 := Now;
   DataSource1.DataSet := ClientModule1.cdsSQLQryEmployee;
   ClientModule1.SQLConn.Close;
   ClientModule1.cdsSQLQryEmployee.Close;
   ClientModule1.cdsSQLQryEmployee.Open;
   ClientModule1.cdsSQLQryEmployee.First;
   while not ClientModule1.cdsSQLQryEmployee.Eof do
      ClientModule1.cdsSQLQryEmployee.Next;
   t2 := Now;
   t3 := t2 - t1;
   ShowMessage('DBX: ' + FormatDateTime('hh:nn:ss:zzz', t3));
end;

procedure TForm3.btnFDClick(Sender: TObject);
var
   t1, t2, t3: TTime;
begin
   t1 := Now;
   DataSource1.DataSet := ClientModule1.cdsFDQryEmployee;
   ClientModule1.SQLConn.Close;
   ClientModule1.cdsFDQryEmployee.Close;

   {
     ClientModule1.cdsFDQryEmployee.Filtered := False;
     ClientModule1.cdsFDQryEmployee.Filter := 'EMP_NO <= 10';
     ClientModule1.cdsFDQryEmployee.Filtered := True;
   }

   ClientModule1.cdsFDQryEmployee.Open;
   ClientModule1.cdsFDQryEmployee.First;
   while not ClientModule1.cdsFDQryEmployee.Eof do
      ClientModule1.cdsFDQryEmployee.Next;
   t2 := Now;
   t3 := t2 - t1;
   ShowMessage('FireDAC: ' + FormatDateTime('hh:nn:ss:zzz', t3));
end;

end.
