object ClientModule1: TClientModule1
  OldCreateOrder = False
  Height = 271
  Width = 415
  object SQLConn: TSQLConnection
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'Port=211'
      'HostName=localhost'
      'CommunicationProtocol=tcp/ip'
      'DSAuthenticationPassword=1234'
      'DSAuthenticationUser=admin'
      'DatasnapContext=datasnap/')
    Left = 32
    Top = 16
  end
  object DSProviderConn: TDSProviderConnection
    ServerClassName = 'TServerMethods1'
    SQLConnection = SQLConn
    Left = 40
    Top = 64
  end
  object cdsFDQryEmployee: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspFDQryEmployee'
    RemoteServer = DSProviderConn
    Left = 72
    Top = 120
  end
  object cdsSQLQryEmployee: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspSQLQryEmployee'
    RemoteServer = DSProviderConn
    Left = 192
    Top = 120
  end
  object cdsFDQryCustomer: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'CUST_NO'
        ParamType = ptInput
      end>
    ProviderName = 'dspFDQryCustomer'
    RemoteServer = DSProviderConn
    Left = 72
    Top = 168
  end
  object cdsSQLQryCustomer: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspSQLQryEmployee'
    RemoteServer = DSProviderConn
    Left = 192
    Top = 168
  end
end
