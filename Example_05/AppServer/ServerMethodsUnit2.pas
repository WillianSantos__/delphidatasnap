unit ServerMethodsUnit2;

interface

uses
   System.SysUtils,
   System.Classes,
   System.Json,
   DataSnap.DSProviderDataModuleAdapter,
   DataSnap.DSServer,
   DataSnap.DSAuth;

type
   TServerMethods2 = class(TDSServerModule)
   private
      { Private declarations }
   public
      { Public declarations }
      function Sum(A, B: Integer): Integer; overload;
      function Sum(A, B: Double): string; overload;
   end;

implementation

{$R *.dfm}

uses System.StrUtils;

function TServerMethods2.Sum(A, B: Integer): Integer;
begin
   Result := A + B;
end;

function TServerMethods2.Sum(A, B: Double): string;
begin
   Result := FloatToStr(A + B);
end;

end.
