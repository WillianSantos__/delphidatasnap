unit ServerMethodsUnit1;

interface

uses
   System.SysUtils,
   System.Classes,
   System.Json,
   DataSnap.DSProviderDataModuleAdapter,
   DataSnap.DSServer,
   DataSnap.DSAuth;

type
   TServerMethods1 = class(TDSServerModule)
   private
      { Private declarations }
   public
      { Public declarations }
      function EchoString(Value: string): string;
      function ReverseString(Value: string): string;
      function Sum(A, B: integer): integer;
   end;

implementation

{$R *.dfm}

uses System.StrUtils;

function TServerMethods1.EchoString(Value: string): string;
begin
   Result := Value;
end;

function TServerMethods1.ReverseString(Value: string): string;
begin
   Result := System.StrUtils.ReverseString(Value);
end;

function TServerMethods1.Sum(A, B: integer): integer;
begin
   Result := A + B;
end;

end.
