object WebModule1: TWebModule1
  OldCreateOrder = False
  OnCreate = WebModuleCreate
  Actions = <
    item
      Default = True
      Name = 'DefaultHandler'
      PathInfo = '/'
      OnAction = WebModule1DefaultHandlerAction
    end>
  Height = 333
  Width = 552
  object DSHTTPWebDispatcher1: TDSHTTPWebDispatcher
    Server = ServerContainer1.DSServer1
    DSPort = 8080
    Filters = <
      item
        FilterId = 'PC1'
        Properties.Strings = (
          'Key=ViESfMUnmkuZeJQ1')
      end
      item
        FilterId = 'RSA'
        Properties.Strings = (
          'UseGlobalKey=true'
          'KeyLength=1024'
          'KeyExponent=3')
      end
      item
        FilterId = 'ZLibCompression'
        Properties.Strings = (
          'CompressMoreThan=1024')
      end>
    AuthenticationManager = ServerContainer1.DSAuthenticationManager1
    WebDispatch.PathInfo = 'datasnap*'
    Left = 96
    Top = 16
  end
  object DSProxyDispatcher1: TDSProxyDispatcher
    DSProxyGenerator = DSProxyGenerator1
    Left = 96
    Top = 160
  end
  object DSProxyGenerator1: TDSProxyGenerator
    MetaDataProvider = DSServerMetaDataProvider1
    TargetUnitName = 'ClientClassesUnit1.pas'
    TargetDirectory = 
      'C:\Users\Edesoft07\Documents\Embarcadero\Studio\Projects\Git\Exa' +
      'mple_05\AppClient'
    Writer = 'Object Pascal REST'
    Left = 96
    Top = 112
  end
  object DSServerMetaDataProvider1: TDSServerMetaDataProvider
    Server = ServerContainer1.DSServer1
    Left = 96
    Top = 64
  end
end
