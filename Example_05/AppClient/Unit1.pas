unit Unit1;

interface

uses
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Classes,
   System.Variants,
   FMX.Types,
   FMX.Controls,
   FMX.Forms,
   FMX.Graphics,
   FMX.Dialogs,
   FMX.Layouts,
   FMX.StdCtrls,
   FMX.Edit,
   FMX.Controls.Presentation;

type
   TFrmClient = class(TForm)
      lblParamA: TLabel;
      edtParamA: TEdit;
      Button1: TButton;
      lytParamA: TLayout;
      lytParamB: TLayout;
      edtParamB: TEdit;
      lblParamB: TLabel;
      ToolBar1: TToolBar;
      Label1: TLabel;
      lytResult: TLayout;
      edtResult: TEdit;
      lblResult: TLabel;
      procedure Button1Click(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   FrmClient: TFrmClient;

implementation

uses
   ClientClassesUnit1,
   ClientModuleUnit1;

{$R *.fmx}

procedure TFrmClient.Button1Click(Sender: TObject);
var
   LParamA: integer;
   LParamB: integer;
   LResult: string;
begin
   LParamA := StrToIntDef(edtParamA.Text, 0);
   LParamB := StrToIntDef(edtParamB.Text, 0);

   try
      LResult := //
         ClientModule1.ServerMethods1Client.Sum(LParamA, LParamB).ToString;

      edtResult.Text := LResult;
   except
      on E: Exception do
      begin
         edtResult.Text := '-1';
         ShowMessage(E.ClassName + sLineBreak + E.Message);
      end;
   end;
end;

end.
