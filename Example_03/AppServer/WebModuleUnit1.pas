unit WebModuleUnit1;

interface

uses
   System.SysUtils,
   System.Classes,
   System.Variants,
   Web.HTTPApp,
   Datasnap.DSHTTPCommon,
   Datasnap.DSHTTPWebBroker,
   Datasnap.DSServer,
   Web.WebFileDispatcher,
   Web.HTTPProd,
   Datasnap.DSAuth,
   Datasnap.DSProxyDispatcher,
   Datasnap.DSProxyJavaAndroid,
   Datasnap.DSProxyJavaBlackBerry,
   Datasnap.DSProxyObjectiveCiOS,
   Datasnap.DSProxyCsharpSilverlight,
   Datasnap.DSProxyFreePascal_iOS,
   Datasnap.DSProxyJavaScript,
   IPPeerServer,
   Datasnap.DSMetadata,
   Datasnap.DSServerMetadata,
   Datasnap.DSClientMetadata,
   Datasnap.DSCommonServer,
   Datasnap.DSHTTP,
   Datasnap.DSSession;

type
   TWebModule1 = class(TWebModule)
      DSHTTPWebDispatcher1: TDSHTTPWebDispatcher;
      ServerFunctionInvoker: TPageProducer;
      ReverseString: TPageProducer;
      WebFileDispatcher1: TWebFileDispatcher;
      DSProxyGenerator1: TDSProxyGenerator;
      DSServerMetaDataProvider1: TDSServerMetaDataProvider;
      DSProxyDispatcher1: TDSProxyDispatcher;
      procedure ServerFunctionInvokerHTMLTag(Sender: TObject; Tag: TTag; const TagString: string; TagParams: TStrings; var ReplaceText: string);
      procedure WebModuleDefaultAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
      procedure WebModuleBeforeDispatch(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
      procedure WebFileDispatcher1BeforeDispatch(Sender: TObject; const AFileName: string; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
      procedure WebModuleCreate(Sender: TObject);
      procedure DSHTTPWebDispatcher1HTTPTrace(Sender: TObject; AContext: TDSHTTPContext; ARequest: TDSHTTPRequest; AResponse: TDSHTTPResponse);
   private
      { Private declarations }
      FServerFunctionInvokerAction: TWebActionItem;
      function AllowServerFunctionInvoker: Boolean;
   public
      { Public declarations }
   end;

var
   WebModuleClass: TComponentClass = TWebModule1;

implementation

{$R *.dfm}

uses ServerMethodsUnit1,
   ServerContainerUnit1,
   Web.WebReq,
   FormUnit1;

procedure TWebModule1.DSHTTPWebDispatcher1HTTPTrace(Sender: TObject; //
   AContext: TDSHTTPContext; ARequest: TDSHTTPRequest; AResponse: TDSHTTPResponse);
   function NowLog: string;
   begin
      Result := '[' + FormatDateTime('yyyy.mm.dd - hh.nn.ss.zzz', Now) + '] ';
   end;

begin
   with Form1.Memo1 do
   begin
      case ARequest.CommandType of
         TDSHTTPCommandType.hcUnknown:
            Lines.Add(NowLog + 'HTTP Trace - Request.CommandType: Unknown');
         TDSHTTPCommandType.hcOther:
            Lines.Add(NowLog + 'HTTP Trace - Request.CommandType: Other');
         TDSHTTPCommandType.hcGET:
            Lines.Add(NowLog + 'HTTP Trace - Request.CommandType: GET');
         TDSHTTPCommandType.hcPOST:
            Lines.Add(NowLog + 'HTTP Trace - Request.CommandType: POST');
         TDSHTTPCommandType.hcDELETE:
            Lines.Add(NowLog + 'HTTP Trace - Request.CommandType: DELETE');
         TDSHTTPCommandType.hcPUT:
            Lines.Add(NowLog + 'HTTP Trace - Request.CommandType: PUT');
      end;

      Lines.Add(NowLog + 'HTTP Trace - Request.RemoteIP: ' + ARequest.RemoteIP);
      Lines.Add(NowLog + 'HTTP Trace - Request.AuthUserName: ' + ARequest.AuthUserName);
      Lines.Add(NowLog + 'HTTP Trace - Request.AuthPassword: ' + ARequest.AuthPassword);

      Lines.Add(NowLog + 'HTTP Trace - Request.Accept: ' + ARequest.Accept);
      Lines.Add(NowLog + 'HTTP Trace - Request.UserAgent: ' + ARequest.UserAgent);

      Lines.Add(NowLog + 'HTTP Trace - Request.Document: ' + ARequest.Document);
      Lines.Add(NowLog + 'HTTP Trace - Request.Command: ' + ARequest.Command);
      Lines.Add(NowLog + 'HTTP Trace - Request.URI: ' + ARequest.URI);
      Lines.Add(NowLog + 'HTTP Trace - Request.ProtocolVersion: ' + ARequest.ProtocolVersion);

      Lines.Add(NowLog + 'HTTP Trace - Response.ResponseNo: ' + AResponse.ResponseNo.ToString);
      Lines.Add(NowLog + 'HTTP Trace - Response.ResponseText: ' + AResponse.ResponseText);
      Lines.Add(NowLog + 'HTTP Trace - Response.ContentText: ' + AResponse.ContentText);
      Lines.Add(NowLog + 'HTTP Trace - Response.ContentLength: ' + AResponse.ContentLength.ToString);
      Lines.Add(EmptyStr);
   end;
end;

procedure TWebModule1.ServerFunctionInvokerHTMLTag(Sender: TObject; Tag: TTag; const TagString: string; TagParams: TStrings; var ReplaceText: string);
begin
   if SameText(TagString, 'urlpath') then
      ReplaceText := string(Request.InternalScriptName)
   else if SameText(TagString, 'port') then
      ReplaceText := IntToStr(Request.ServerPort)
   else if SameText(TagString, 'host') then
      ReplaceText := string(Request.Host)
   else if SameText(TagString, 'classname') then
      ReplaceText := ServerMethodsUnit1.TServerMethods1.ClassName
   else if SameText(TagString, 'loginrequired') then
      if DSHTTPWebDispatcher1.AuthenticationManager <> nil then
         ReplaceText := 'true'
      else
         ReplaceText := 'false'
   else if SameText(TagString, 'serverfunctionsjs') then
      ReplaceText := string(Request.InternalScriptName) + '/js/serverfunctions.js'
   else if SameText(TagString, 'servertime') then
      ReplaceText := DateTimeToStr(Now)
   else if SameText(TagString, 'serverfunctioninvoker') then
      if AllowServerFunctionInvoker then
         ReplaceText := '<div><a href="' + string(Request.InternalScriptName) + '/ServerFunctionInvoker" target="_blank">Server Functions</a></div>'
      else
         ReplaceText := '';
end;

procedure TWebModule1.WebModuleDefaultAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
begin
   if (Request.InternalPathInfo = '') or (Request.InternalPathInfo = '/') then
      Response.Content := ReverseString.Content
   else
      Response.SendRedirect(Request.InternalScriptName + '/');

end;

procedure TWebModule1.WebModuleBeforeDispatch(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
begin
   if FServerFunctionInvokerAction <> nil then
      FServerFunctionInvokerAction.Enabled := AllowServerFunctionInvoker;
end;

function TWebModule1.AllowServerFunctionInvoker: Boolean;
begin
   Result := (Request.RemoteAddr = '127.0.0.1') or (Request.RemoteAddr = '0:0:0:0:0:0:0:1') or (Request.RemoteAddr = '::1');
end;

procedure TWebModule1.WebFileDispatcher1BeforeDispatch(Sender: TObject; const AFileName: string; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
   D1, D2: TDateTime;
begin
   Handled := False;
   if SameFileName(ExtractFileName(AFileName), 'serverfunctions.js') then
      if not FileExists(AFileName) or (FileAge(AFileName, D1) and FileAge(WebApplicationFileName, D2) and (D1 < D2)) then
      begin
         DSProxyGenerator1.TargetDirectory := ExtractFilePath(AFileName);
         DSProxyGenerator1.TargetUnitName := ExtractFileName(AFileName);
         DSProxyGenerator1.Write;
      end;
end;

procedure TWebModule1.WebModuleCreate(Sender: TObject);
begin
   FServerFunctionInvokerAction := ActionByName('ServerFunctionInvokerAction');
   DSServerMetaDataProvider1.Server := DSServer;
   DSHTTPWebDispatcher1.Server := DSServer;
   if DSServer.Started then
   begin
      DSHTTPWebDispatcher1.DbxContext := DSServer.DbxContext;
      DSHTTPWebDispatcher1.Start;
   end;
   DSHTTPWebDispatcher1.AuthenticationManager := DSAuthenticationManager;
end;

initialization

finalization

Web.WebReq.FreeWebModules;

end.
