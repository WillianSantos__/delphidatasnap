unit ServerMethodsUnit1;

interface

uses System.SysUtils,
   System.Classes,
   System.Json,
   DataSnap.DSProviderDataModuleAdapter,
   DataSnap.DSServer,
   DataSnap.DSAuth,
   FireDAC.Stan.ExprFuncs,
   FireDAC.Phys.SQLiteDef,
   FireDAC.UI.Intf,
   FireDAC.VCLUI.Wait,
   FireDAC.Stan.Intf,
   FireDAC.Stan.Option,
   FireDAC.Stan.Error,
   FireDAC.Phys.Intf,
   FireDAC.Stan.Def,
   FireDAC.Stan.Pool,
   FireDAC.Stan.Async,
   FireDAC.Phys,
   FireDAC.Phys.SQLite,
   FireDAC.Stan.Param,
   FireDAC.DatS,
   FireDAC.DApt.Intf,
   FireDAC.DApt,
   Data.DB,
   FireDAC.Comp.DataSet,
   FireDAC.Comp.Client,
   FireDAC.Stan.StorageJSON,
   FireDAC.Stan.StorageBin,
   FireDAC.Comp.UI,
   Data.FireDACJSONReflect;

type
   TServerMethods1 = class(TDSServerModule)
      FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink;
      FDGUIxWaitCursor1: TFDGUIxWaitCursor;
      FDStanStorageBinLink1: TFDStanStorageBinLink;
      FDStanStorageJSONLink1: TFDStanStorageJSONLink;
      FDConnection1: TFDConnection;
      FDQuery1: TFDQuery;
   private
      { Private declarations }
   public
      { Public declarations }
      function EchoString(Value: string): string;
      function ReverseString(Value: string): string;

      function ClienteLista: TFDJSONDataSets;

      function Cliente(ATipo, ACodigo: integer; //
         ARazaoSocial, ACNPJ: string): Boolean;
   end;

implementation

{$R *.dfm}

uses System.StrUtils,
   FormUnit1;

function TServerMethods1.ClienteLista: TFDJSONDataSets;
begin
   // Alunos Favor Implementar
end;

function TServerMethods1.Cliente( //
   ATipo, ACodigo: integer; ARazaoSocial, ACNPJ: string): Boolean;
var
   LSQL: string;
begin
   case ATipo of
      1: { todo: Inserir }
         begin
            LSQL := //
               Format('INSERT INTO TB_CLIENTE (DS_RAZAO, DS_CNPJ) VALUES (%s, %s)' //
               , [QuotedStr(Copy(ARazaoSocial, 1, 254)), QuotedStr(Copy(ACNPJ, 1, 18))]);
         end;

      2: { todo: Alterar }
         begin
            LSQL := //
               Format('UPDATE TB_CLIENTE SET DS_RAZAO = %s, DS_CNPJ = %s WHERE ID_CLIENTE = %s' //
               , [QuotedStr(Copy(ARazaoSocial, 1, 254)), QuotedStr(Copy(ACNPJ, 1, 18)), ACodigo.ToString]);
         end;

      3: { todo: Excluir }
         begin
            LSQL := //
               Format('DELETE FROM TB_CLIENTE WHERE ID_CLIENTE = %s' //
               , [ACodigo.ToString]);
         end;
   end;

   try
      if not(LSQL = EmptyStr) then
      begin
         try
            FDQuery1.ExecSQL(LSQL);
         except
            on E: Exception do { todo: Exce��o, caso erro com a SQL }
            begin
               Form1.Memo1.Lines.Add(Result.ToString);
            end;
         end;
      end;
      Result := True;
   except
      on E: Exception do { todo: Exce��o, caso erro com a m�todo }
      begin
         Form1.Memo1.Lines.Add(Result.ToString);
         Result := False;
      end;
   end;
end;

function TServerMethods1.EchoString(Value: string): string;
begin
   Result := Value;
end;

function TServerMethods1.ReverseString(Value: string): string;
begin
   Result := System.StrUtils.ReverseString(Value);
end;

end.
