// 
// Created by the DataSnap proxy generator.
// 30/09/2017 11:49:58
// 

unit ClientClassesUnit1;

interface

uses System.JSON, Datasnap.DSProxyRest, Datasnap.DSClientRest, Data.DBXCommon, Data.DBXClient, Data.DBXDataSnap, Data.DBXJSON, Datasnap.DSProxy, System.Classes, System.SysUtils, Data.DB, Data.SqlExpr, Data.DBXDBReaders, Data.DBXCDSReaders, Data.FireDACJSONReflect, Data.DBXJSONReflect;

type

  IDSRestCachedTFDJSONDataSets = interface;

  TServerMethods1Client = class(TDSAdminRestClient)
  private
    FEchoStringCommand: TDSRestCommand;
    FReverseStringCommand: TDSRestCommand;
    FClienteListaCommand: TDSRestCommand;
    FClienteListaCommand_Cache: TDSRestCommand;
    FClienteCommand: TDSRestCommand;
  public
    constructor Create(ARestConnection: TDSRestConnection); overload;
    constructor Create(ARestConnection: TDSRestConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function EchoString(Value: string; const ARequestFilter: string = ''): string;
    function ReverseString(Value: string; const ARequestFilter: string = ''): string;
    function ClienteLista(const ARequestFilter: string = ''): TFDJSONDataSets;
    function ClienteLista_Cache(const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function Cliente(ATipo: Integer; ACodigo: Integer; ARazaoSocial: string; ACNPJ: string; const ARequestFilter: string = ''): Boolean;
  end;

  IDSRestCachedTFDJSONDataSets = interface(IDSRestCachedObject<TFDJSONDataSets>)
  end;

  TDSRestCachedTFDJSONDataSets = class(TDSRestCachedObject<TFDJSONDataSets>, IDSRestCachedTFDJSONDataSets, IDSRestCachedCommand)
  end;

const
  TServerMethods1_EchoString: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Value'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

  TServerMethods1_ReverseString: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Value'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

  TServerMethods1_ClienteLista: array [0..0] of TDSRestParameterMetaData =
  (
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TServerMethods1_ClienteLista_Cache: array [0..0] of TDSRestParameterMetaData =
  (
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethods1_Cliente: array [0..4] of TDSRestParameterMetaData =
  (
    (Name: 'ATipo'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'ACodigo'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'ARazaoSocial'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'ACNPJ'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 4; TypeName: 'Boolean')
  );

implementation

function TServerMethods1Client.EchoString(Value: string; const ARequestFilter: string): string;
begin
  if FEchoStringCommand = nil then
  begin
    FEchoStringCommand := FConnection.CreateCommand;
    FEchoStringCommand.RequestType := 'GET';
    FEchoStringCommand.Text := 'TServerMethods1.EchoString';
    FEchoStringCommand.Prepare(TServerMethods1_EchoString);
  end;
  FEchoStringCommand.Parameters[0].Value.SetWideString(Value);
  FEchoStringCommand.Execute(ARequestFilter);
  Result := FEchoStringCommand.Parameters[1].Value.GetWideString;
end;

function TServerMethods1Client.ReverseString(Value: string; const ARequestFilter: string): string;
begin
  if FReverseStringCommand = nil then
  begin
    FReverseStringCommand := FConnection.CreateCommand;
    FReverseStringCommand.RequestType := 'GET';
    FReverseStringCommand.Text := 'TServerMethods1.ReverseString';
    FReverseStringCommand.Prepare(TServerMethods1_ReverseString);
  end;
  FReverseStringCommand.Parameters[0].Value.SetWideString(Value);
  FReverseStringCommand.Execute(ARequestFilter);
  Result := FReverseStringCommand.Parameters[1].Value.GetWideString;
end;

function TServerMethods1Client.ClienteLista(const ARequestFilter: string): TFDJSONDataSets;
begin
  if FClienteListaCommand = nil then
  begin
    FClienteListaCommand := FConnection.CreateCommand;
    FClienteListaCommand.RequestType := 'GET';
    FClienteListaCommand.Text := 'TServerMethods1.ClienteLista';
    FClienteListaCommand.Prepare(TServerMethods1_ClienteLista);
  end;
  FClienteListaCommand.Execute(ARequestFilter);
  if not FClienteListaCommand.Parameters[0].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FClienteListaCommand.Parameters[0].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FClienteListaCommand.Parameters[0].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FClienteListaCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TServerMethods1Client.ClienteLista_Cache(const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FClienteListaCommand_Cache = nil then
  begin
    FClienteListaCommand_Cache := FConnection.CreateCommand;
    FClienteListaCommand_Cache.RequestType := 'GET';
    FClienteListaCommand_Cache.Text := 'TServerMethods1.ClienteLista';
    FClienteListaCommand_Cache.Prepare(TServerMethods1_ClienteLista_Cache);
  end;
  FClienteListaCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FClienteListaCommand_Cache.Parameters[0].Value.GetString);
end;

function TServerMethods1Client.Cliente(ATipo: Integer; ACodigo: Integer; ARazaoSocial: string; ACNPJ: string; const ARequestFilter: string): Boolean;
begin
  if FClienteCommand = nil then
  begin
    FClienteCommand := FConnection.CreateCommand;
    FClienteCommand.RequestType := 'GET';
    FClienteCommand.Text := 'TServerMethods1.Cliente';
    FClienteCommand.Prepare(TServerMethods1_Cliente);
  end;
  FClienteCommand.Parameters[0].Value.SetInt32(ATipo);
  FClienteCommand.Parameters[1].Value.SetInt32(ACodigo);
  FClienteCommand.Parameters[2].Value.SetWideString(ARazaoSocial);
  FClienteCommand.Parameters[3].Value.SetWideString(ACNPJ);
  FClienteCommand.Execute(ARequestFilter);
  Result := FClienteCommand.Parameters[4].Value.GetBoolean;
end;

constructor TServerMethods1Client.Create(ARestConnection: TDSRestConnection);
begin
  inherited Create(ARestConnection);
end;

constructor TServerMethods1Client.Create(ARestConnection: TDSRestConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ARestConnection, AInstanceOwner);
end;

destructor TServerMethods1Client.Destroy;
begin
  FEchoStringCommand.DisposeOf;
  FReverseStringCommand.DisposeOf;
  FClienteListaCommand.DisposeOf;
  FClienteListaCommand_Cache.DisposeOf;
  FClienteCommand.DisposeOf;
  inherited;
end;

end.
