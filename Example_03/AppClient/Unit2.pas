unit Unit2;

interface

uses
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Classes,
   System.Variants,
   FMX.Types,
   FMX.Controls,
   FMX.Forms,
   FMX.Graphics,
   FMX.Dialogs,
   FMX.Edit,
   FMX.Controls.Presentation,
   FMX.StdCtrls,
   FMX.ListBox,
   FMX.ListView.Types,
   FMX.ListView.Appearances,
   FMX.ListView.Adapters.Base,
   FMX.ListView;

type
   TForm2 = class(TForm)
      Label1: TLabel;
      Edit1: TEdit;
      Edit2: TEdit;
      Label2: TLabel;
      Edit3: TEdit;
      Label3: TLabel;
      ComboBox1: TComboBox;
      Label4: TLabel;
      Button1: TButton;
      Button2: TButton;
      ListView1: TListView;
      Label5: TLabel;
      procedure Button1Click(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form2: TForm2;

implementation

uses
   ClientClassesUnit1,
   ClientModuleUnit1;

{$R *.fmx}

procedure TForm2.Button1Click(Sender: TObject);
var
   SM: TServerMethods1Client;
   bCrud: Boolean;
begin
   SM := TServerMethods1Client.Create(ClientModule1.DSRestConnection1);

   try
      bCrud := SM.Cliente( //
         Self.ComboBox1.ItemIndex // Paramatro: Tipo do CRUD
         , StrToInt(Self.Edit1.Text) // Paramatro: Codigo
         , Self.Edit2.Text // Paramatro: Razao Social
         , Self.Edit3.Text // Paramatro: CNPJ
         );

      if bCrud then
         ShowMessage(ComboBox1.Items.Strings[ComboBox1.ItemIndex] + ' OK')
      else
         ShowMessage(ComboBox1.Items.Strings[ComboBox1.ItemIndex] + ' Falhou');

   finally
      SM.Free;
   end;
end;

end.
