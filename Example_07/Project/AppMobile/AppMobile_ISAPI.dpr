program AppMobile_ISAPI;

uses
  System.StartUpCopy,
  FMX.Forms,
  Unit1 in 'Unit1.pas' {Form1},
  ClientClassesUnit2 in 'Module\ISAPI\ClientClassesUnit2.pas',
  ClientModuleUnit2 in 'Module\ISAPI\ClientModuleUnit2.pas' {ClientModule2: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TClientModule2, ClientModule2);
  Application.Run;
end.
