unit Unit1;

interface

uses
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Classes,
   System.Variants,
   FMX.Types,
   FMX.Controls,
   FMX.Forms,
   FMX.Graphics,
   FMX.Dialogs,
   FMX.StdCtrls,
   FMX.Controls.Presentation,
   FMX.TabControl,
   FMX.Layouts,
   FMX.Edit;

type
   TForm1 = class(TForm)
      ToolBar1: TToolBar;
      Label1: TLabel;
      TabControl1: TTabControl;
      TabItem1: TTabItem;
      TabItem2: TTabItem;
      Layout1: TLayout;
      Label2: TLabel;
      Edit1: TEdit;
      Layout2: TLayout;
      Label3: TLabel;
      Edit2: TEdit;
      Layout3: TLayout;
      Button1: TButton;
      procedure Button1Click(Sender: TObject);
      procedure FormCreate(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form1: TForm1;

implementation

{$R *.fmx}

uses
   ClientModuleUnit1;

procedure TForm1.Button1Click(Sender: TObject);
var
   CM: TClientModule1;

   LEmail: string;
   LPwd_D: string;

   LName: string;
begin
   LEmail := Edit1.Text;
   LPwd_D := Edit2.Text;

   CM := TClientModule1.Create(Self);

   try
      try
         CM.DSRestConnection1.LoginPrompt := False;
         CM.DSRestConnection1.UserName := LEmail;
         CM.DSRestConnection1.Password := LPwd_D;

         LName := CM.ServerMethods1Client.Login(LEmail, LPwd_D);

         if not(LName = EmptyStr) then
         begin
            TabControl1.ActiveTab := TabItem2;
            Label1.Text := LName + ' (Online)';
         end;

      except
         on E: Exception do
         begin
            ShowMessage(E.ClassName + '. ' + E.Message);
         end;
      end;

   finally
      FreeAndNil(CM);
   end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
   TabControl1.ActiveTab := TabItem1;
end;

end.
