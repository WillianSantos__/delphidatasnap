unit ServerMethodsUnit1;

interface

uses
   System.SysUtils,
   System.Classes,
   System.Json,
   DataSnap.DSProviderDataModuleAdapter,
   DataSnap.DSServer,
   DataSnap.DSAuth,
   FireDAC.Phys.FBDef,
   FireDAC.UI.Intf,
   FireDAC.FMXUI.Wait,
   FireDAC.Comp.UI,
   FireDAC.Stan.Intf,
   FireDAC.Phys,
   FireDAC.Phys.IBBase,
   FireDAC.Phys.FB,
   FireDAC.Stan.Option,
   FireDAC.Stan.Error,
   FireDAC.Phys.Intf,
   FireDAC.Stan.Def,
   FireDAC.Stan.Pool,
   FireDAC.Stan.Async,
   Data.DB,
   FireDAC.Comp.Client,
   FireDAC.Stan.Param,
   FireDAC.DatS,
   FireDAC.DApt.Intf,
   FireDAC.DApt,
   FireDAC.Comp.DataSet;

type
   {$METHODINFO ON}
   TServerMethods1 = class(TDSServerModule)
      FDPhysFBDriverLink: TFDPhysFBDriverLink;
      FDGUIxWaitCursor: TFDGUIxWaitCursor;
      FDConn: TFDConnection;
      FDQryEmployeeAccess: TFDQuery;
   private
      { Private declarations }
   public
      { Public declarations }
      function EchoString(Value: string): string;
      function ReverseString(Value: string): string;

      function Login(AEmail, APassword: string): string;

   end;
   {$METHODINFO OFF}

var
   ServerMethods1: TServerMethods1;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}
{$R *.dfm}

uses System.StrUtils;

function TServerMethods1.EchoString(Value: string): string;
begin
   Result := Value;
end;

function TServerMethods1.ReverseString(Value: string): string;
begin
   Result := System.StrUtils.ReverseString(Value);
end;

function TServerMethods1.Login(AEmail, APassword: string): string;
begin
   FDQryEmployeeAccess.Close;
   FDQryEmployeeAccess.Params.ParamValues['EMAIL'] := AnsiLowerCase(AEmail);
   FDQryEmployeeAccess.Params.ParamValues['PWD_D'] := AnsiLowerCase(APassword);
   FDQryEmployeeAccess.Open;

   if not FDQryEmployeeAccess.IsEmpty then
   begin
      Result := FDQryEmployeeAccess.Fields[0].AsString;
   end
   else
   begin
      Result := EmptyStr;
   end;
end;

end.
