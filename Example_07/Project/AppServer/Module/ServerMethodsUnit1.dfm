object ServerMethods1: TServerMethods1
  OldCreateOrder = False
  Height = 324
  Width = 509
  object FDPhysFBDriverLink: TFDPhysFBDriverLink
    Left = 64
    Top = 8
  end
  object FDGUIxWaitCursor: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 64
    Top = 56
  end
  object FDConn: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\Edesoft07\Documents\Embarcadero\Studio\Project' +
        's\Git\_Database\EMPLOYEE.FDB'
      'User_Name=sysdba'
      'Password=masterkey'
      'Protocol=TCPIP'
      'Server=localhost'
      'Port=3050'
      'CharacterSet=UTF8'
      'OpenMode=OpenOrCreate'
      'DriverID=FB')
    LoginPrompt = False
    Left = 64
    Top = 104
  end
  object FDQryEmployeeAccess: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'SELECT '
      'UPPER(FIRST_NAME || '#39' '#39' || LAST_NAME) AS FULLNAME'
      'FROM EMPLOYEE'
      'WHERE 1 = 1'
      'AND EMAIL = :EMAIL'
      'AND PWD_D = :PWD_D')
    Left = 64
    Top = 152
    ParamData = <
      item
        Name = 'EMAIL'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'PWD_D'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end>
  end
end
