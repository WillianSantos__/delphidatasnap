unit FormUnit1;

interface

uses
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Classes,
   System.Variants,
   FMX.Types,
   FMX.Graphics,
   FMX.Controls,
   FMX.Forms,
   FMX.Dialogs,
   FMX.StdCtrls,
   FMX.Edit,
   IdHTTPWebBrokerBridge,
   Web.HTTPApp,
   FMX.Controls.Presentation,
   IdBaseComponent,
   IdComponent,
   IdIPWatch,
   FMX.ScrollBox,
   FMX.Memo;

type
   TForm1 = class(TForm)
      ButtonStart: TButton;
      ButtonStop: TButton;
      EditPort: TEdit;
      Label1: TLabel;
      ButtonOpenBrowser: TButton;
      IdIPWatch1: TIdIPWatch;
      EditHost: TEdit;
      Label2: TLabel;
      Memo1: TMemo;
      procedure FormCreate(Sender: TObject);
      procedure ButtonStartClick(Sender: TObject);
      procedure ButtonStopClick(Sender: TObject);
      procedure ButtonOpenBrowserClick(Sender: TObject);
      procedure FormShow(Sender: TObject);
   private
      FServer: TIdHTTPWebBrokerBridge;
      procedure StartServer;
      procedure ApplicationIdle(Sender: TObject; var Done: Boolean);
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form1: TForm1;

implementation

{$R *.fmx}

uses
   WinApi.Windows,
   WinApi.ShellApi,
   Datasnap.DSSession;

procedure TForm1.ApplicationIdle(Sender: TObject; var Done: Boolean);
begin
   ButtonStart.Enabled := not FServer.Active;
   ButtonStop.Enabled := FServer.Active;
   EditHost.Enabled := not FServer.Active;
   EditPort.Enabled := not FServer.Active;
end;

procedure TForm1.ButtonOpenBrowserClick(Sender: TObject);
var
   LURL: string;
begin
   StartServer;
   LURL := Format('http://localhost:%s', [EditPort.Text]);
   ShellExecute(0, nil, PChar(LURL), nil, nil, SW_SHOWNOACTIVATE);
end;

procedure TForm1.ButtonStartClick(Sender: TObject);
begin
   StartServer;
end;

procedure TerminateThreads;
begin
   if TDSSessionManager.Instance <> nil then
      TDSSessionManager.Instance.TerminateAllSessions;
end;

procedure TForm1.ButtonStopClick(Sender: TObject);
begin
   TerminateThreads;
   FServer.Active := False;
   FServer.Bindings.Clear;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
   Self.Caption := 'Server DataSnap REST';

   {$IFDEF MSWINDOWS}
   Self.Height := 455;
   Self.Width := 555;
   Self.ClientHeight := Self.Height;
   Self.ClientWidth := Self.Width;
   Self.BorderStyle := TFmxFormBorderStyle.Single;
   {$ENDIF}
   FServer := TIdHTTPWebBrokerBridge.Create(Self);
   Application.OnIdle := ApplicationIdle;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
   IdIPWatch1.Active := False;
   IdIPWatch1.HistoryEnabled := False;
   IdIPWatch1.HistoryFilename := EmptyStr;
   IdIPWatch1.Active := True;
   EditHost.Text := IdIPWatch1.LocalIP;

   Self.StartServer;
end;

procedure TForm1.StartServer;
begin
   if not FServer.Active then
   begin
      FServer.Bindings.Clear;
      FServer.DefaultPort := StrToInt(EditPort.Text);
      FServer.Active := True;
   end;
end;

end.
