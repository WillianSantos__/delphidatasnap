object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 201
  ClientWidth = 447
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 56
    Top = 40
    Width = 34
    Height = 13
    Caption = 'Valor A'
  end
  object Label2: TLabel
    Left = 56
    Top = 128
    Width = 33
    Height = 13
    Caption = 'Valor B'
  end
  object Label3: TLabel
    Left = 240
    Top = 80
    Width = 48
    Height = 13
    Caption = 'Resultado'
  end
  object Edit1: TEdit
    Left = 56
    Top = 56
    Width = 121
    Height = 21
    TabOrder = 0
    Text = '8'
  end
  object Edit2: TEdit
    Left = 56
    Top = 144
    Width = 121
    Height = 21
    TabOrder = 1
    Text = '2'
  end
  object Edit3: TEdit
    Left = 240
    Top = 96
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '0'
  end
  object Button1: TButton
    Left = 240
    Top = 142
    Width = 121
    Height = 25
    Caption = 'Resultado'
    TabOrder = 3
    OnClick = Button1Click
  end
end
