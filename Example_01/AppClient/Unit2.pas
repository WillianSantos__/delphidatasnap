unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm2 = class(TForm)
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

uses
   ClientModuleUnit1;

{$R *.dfm}

procedure TForm2.Button1Click(Sender: TObject);
var
   Temp: TClientModule1;
   A, B: integer;
begin
   Temp := TClientModule1.Create(Self);
   try
      A := StrToInt(Edit1.Text);
      B := StrToInt(Edit2.Text);

      Edit3.Text := IntToStr(Temp.ServerMethods1Client.Sum(A, B));
   finally
      Temp.Free;
   end;
end;

end.
