unit Unit1;

interface

uses Winapi.Windows,
   Winapi.Messages,
   System.SysUtils,
   System.Variants,
   System.Classes,
   Vcl.Graphics,
   Vcl.Controls,
   Vcl.Forms,
   Vcl.Dialogs,
   Vcl.StdCtrls,
   IdBaseComponent,
   IdComponent,
   IdIPWatch;

type
   TForm1 = class(TForm)
      IdIPWatch1: TIdIPWatch;
      Label1: TLabel;
      procedure FormShow(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form1: TForm1;

implementation

{$R *.dfm}

uses ServerContainerUnit1;

procedure TForm1.FormShow(Sender: TObject);
begin
   IdIPWatch1.Active := False;
   IdIPWatch1.HistoryFilename := EmptyStr;
   IdIPWatch1.HistoryEnabled := False;
   IdIPWatch1.Active := True;

   Label1.Caption := //
      'IP/Porta do Servidor DataSnap: ' + //
      IdIPWatch1.LocalIP + ':' + //
      ServerContainer1.DSTCPServerTransport1.Port.ToString;
end;

end.
