object ClientModule1: TClientModule1
  OldCreateOrder = False
  Height = 271
  Width = 415
  object DSRestConnection1: TDSRestConnection
    Host = '192.168.0.21'
    Port = 8080
    LoginPrompt = False
    Left = 56
    Top = 16
    UniqueId = '{2D3210A4-D96C-40A1-A548-DA75902D1AF9}'
  end
  object FDStanStorageJSONLink1: TFDStanStorageJSONLink
    Left = 320
    Top = 8
  end
  object FDStanStorageXMLLink1: TFDStanStorageXMLLink
    Left = 320
    Top = 56
  end
  object FDStanStorageBinLink1: TFDStanStorageBinLink
    Left = 320
    Top = 104
  end
  object FDMemTable1: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 56
    Top = 64
  end
end
