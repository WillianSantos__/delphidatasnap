//
// Created by the DataSnap proxy generator.
// 24/03/2018 16:20:19
//

unit ClientClassesUnit1;

interface

uses System.JSON, Datasnap.DSProxyRest, Datasnap.DSClientRest, Data.DBXCommon, Data.DBXClient, Data.DBXDataSnap, Data.DBXJSON, Datasnap.DSProxy, System.Classes, System.SysUtils, Data.DB, Data.SqlExpr, Data.DBXDBReaders, Data.DBXCDSReaders, Data.FireDACJSONReflect, Data.DBXJSONReflect;

type

  IDSRestCachedTFDJSONDataSets = interface;

  TServerMethods1Client = class(TDSAdminRestClient)
  private
    FEchoStringCommand: TDSRestCommand;
    FReverseStringCommand: TDSRestCommand;
    FCustomerListCommand: TDSRestCommand;
    FCustomerListCommand_Cache: TDSRestCommand;
    FCustomerListIdCommand: TDSRestCommand;
    FCustomerListIdCommand_Cache: TDSRestCommand;
    FCustomerInsertCommand: TDSRestCommand;
    FCustomerUpdateCommand: TDSRestCommand;
    FCustomerDeleteCommand: TDSRestCommand;
  public
    constructor Create(ARestConnection: TDSRestConnection); overload;
    constructor Create(ARestConnection: TDSRestConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function EchoString(Value: string; const ARequestFilter: string = ''): string;
    function ReverseString(Value: string; const ARequestFilter: string = ''): string;
    function CustomerList(const ARequestFilter: string = ''): TFDJSONDataSets;
    function CustomerList_Cache(const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function CustomerListId(AId: Integer; const ARequestFilter: string = ''): TFDJSONDataSets;
    function CustomerListId_Cache(AId: Integer; const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function CustomerInsert(AId: Integer; ACustomer: string; const ARequestFilter: string = ''): Boolean;
    function CustomerUpdate(AId: Integer; ACustomer: string; AKey: Integer; const ARequestFilter: string = ''): Boolean;
    function CustomerDelete(AKey: Integer; const ARequestFilter: string = ''): Boolean;
  end;

  IDSRestCachedTFDJSONDataSets = interface(IDSRestCachedObject<TFDJSONDataSets>)
  end;

  TDSRestCachedTFDJSONDataSets = class(TDSRestCachedObject<TFDJSONDataSets>, IDSRestCachedTFDJSONDataSets, IDSRestCachedCommand)
  end;

const
  TServerMethods1_EchoString: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Value'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

  TServerMethods1_ReverseString: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Value'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

  TServerMethods1_CustomerList: array [0..0] of TDSRestParameterMetaData =
  (
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TServerMethods1_CustomerList_Cache: array [0..0] of TDSRestParameterMetaData =
  (
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethods1_CustomerListId: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'AId'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TServerMethods1_CustomerListId_Cache: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'AId'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethods1_CustomerInsert: array [0..2] of TDSRestParameterMetaData =
  (
    (Name: 'AId'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'ACustomer'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 4; TypeName: 'Boolean')
  );

  TServerMethods1_CustomerUpdate: array [0..3] of TDSRestParameterMetaData =
  (
    (Name: 'AId'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'ACustomer'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'AKey'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 4; TypeName: 'Boolean')
  );

  TServerMethods1_CustomerDelete: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'AKey'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 4; TypeName: 'Boolean')
  );

implementation

function TServerMethods1Client.EchoString(Value: string; const ARequestFilter: string): string;
begin
  if FEchoStringCommand = nil then
  begin
    FEchoStringCommand := FConnection.CreateCommand;
    FEchoStringCommand.RequestType := 'GET';
    FEchoStringCommand.Text := 'TServerMethods1.EchoString';
    FEchoStringCommand.Prepare(TServerMethods1_EchoString);
  end;
  FEchoStringCommand.Parameters[0].Value.SetWideString(Value);
  FEchoStringCommand.Execute(ARequestFilter);
  Result := FEchoStringCommand.Parameters[1].Value.GetWideString;
end;

function TServerMethods1Client.ReverseString(Value: string; const ARequestFilter: string): string;
begin
  if FReverseStringCommand = nil then
  begin
    FReverseStringCommand := FConnection.CreateCommand;
    FReverseStringCommand.RequestType := 'GET';
    FReverseStringCommand.Text := 'TServerMethods1.ReverseString';
    FReverseStringCommand.Prepare(TServerMethods1_ReverseString);
  end;
  FReverseStringCommand.Parameters[0].Value.SetWideString(Value);
  FReverseStringCommand.Execute(ARequestFilter);
  Result := FReverseStringCommand.Parameters[1].Value.GetWideString;
end;

function TServerMethods1Client.CustomerList(const ARequestFilter: string): TFDJSONDataSets;
begin
  if FCustomerListCommand = nil then
  begin
    FCustomerListCommand := FConnection.CreateCommand;
    FCustomerListCommand.RequestType := 'GET';
    FCustomerListCommand.Text := 'TServerMethods1.CustomerList';
    FCustomerListCommand.Prepare(TServerMethods1_CustomerList);
  end;
  FCustomerListCommand.Execute(ARequestFilter);
  if not FCustomerListCommand.Parameters[0].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FCustomerListCommand.Parameters[0].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FCustomerListCommand.Parameters[0].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FCustomerListCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TServerMethods1Client.CustomerList_Cache(const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FCustomerListCommand_Cache = nil then
  begin
    FCustomerListCommand_Cache := FConnection.CreateCommand;
    FCustomerListCommand_Cache.RequestType := 'GET';
    FCustomerListCommand_Cache.Text := 'TServerMethods1.CustomerList';
    FCustomerListCommand_Cache.Prepare(TServerMethods1_CustomerList_Cache);
  end;
  FCustomerListCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FCustomerListCommand_Cache.Parameters[0].Value.GetString);
end;

function TServerMethods1Client.CustomerListId(AId: Integer; const ARequestFilter: string): TFDJSONDataSets;
begin
  if FCustomerListIdCommand = nil then
  begin
    FCustomerListIdCommand := FConnection.CreateCommand;
    FCustomerListIdCommand.RequestType := 'GET';
    FCustomerListIdCommand.Text := 'TServerMethods1.CustomerListId';
    FCustomerListIdCommand.Prepare(TServerMethods1_CustomerListId);
  end;
  FCustomerListIdCommand.Parameters[0].Value.SetInt32(AId);
  FCustomerListIdCommand.Execute(ARequestFilter);
  if not FCustomerListIdCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FCustomerListIdCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FCustomerListIdCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FCustomerListIdCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TServerMethods1Client.CustomerListId_Cache(AId: Integer; const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FCustomerListIdCommand_Cache = nil then
  begin
    FCustomerListIdCommand_Cache := FConnection.CreateCommand;
    FCustomerListIdCommand_Cache.RequestType := 'GET';
    FCustomerListIdCommand_Cache.Text := 'TServerMethods1.CustomerListId';
    FCustomerListIdCommand_Cache.Prepare(TServerMethods1_CustomerListId_Cache);
  end;
  FCustomerListIdCommand_Cache.Parameters[0].Value.SetInt32(AId);
  FCustomerListIdCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FCustomerListIdCommand_Cache.Parameters[1].Value.GetString);
end;

function TServerMethods1Client.CustomerInsert(AId: Integer; ACustomer: string; const ARequestFilter: string): Boolean;
begin
  if FCustomerInsertCommand = nil then
  begin
    FCustomerInsertCommand := FConnection.CreateCommand;
    FCustomerInsertCommand.RequestType := 'GET';
    FCustomerInsertCommand.Text := 'TServerMethods1.CustomerInsert';
    FCustomerInsertCommand.Prepare(TServerMethods1_CustomerInsert);
  end;
  FCustomerInsertCommand.Parameters[0].Value.SetInt32(AId);
  FCustomerInsertCommand.Parameters[1].Value.SetWideString(ACustomer);
  FCustomerInsertCommand.Execute(ARequestFilter);
  Result := FCustomerInsertCommand.Parameters[2].Value.GetBoolean;
end;

function TServerMethods1Client.CustomerUpdate(AId: Integer; ACustomer: string; AKey: Integer; const ARequestFilter: string): Boolean;
begin
  if FCustomerUpdateCommand = nil then
  begin
    FCustomerUpdateCommand := FConnection.CreateCommand;
    FCustomerUpdateCommand.RequestType := 'GET';
    FCustomerUpdateCommand.Text := 'TServerMethods1.CustomerUpdate';
    FCustomerUpdateCommand.Prepare(TServerMethods1_CustomerUpdate);
  end;
  FCustomerUpdateCommand.Parameters[0].Value.SetInt32(AId);
  FCustomerUpdateCommand.Parameters[1].Value.SetWideString(ACustomer);
  FCustomerUpdateCommand.Parameters[2].Value.SetInt32(AKey);
  FCustomerUpdateCommand.Execute(ARequestFilter);
  Result := FCustomerUpdateCommand.Parameters[3].Value.GetBoolean;
end;

function TServerMethods1Client.CustomerDelete(AKey: Integer; const ARequestFilter: string): Boolean;
begin
  if FCustomerDeleteCommand = nil then
  begin
    FCustomerDeleteCommand := FConnection.CreateCommand;
    FCustomerDeleteCommand.RequestType := 'GET';
    FCustomerDeleteCommand.Text := 'TServerMethods1.CustomerDelete';
    FCustomerDeleteCommand.Prepare(TServerMethods1_CustomerDelete);
  end;
  FCustomerDeleteCommand.Parameters[0].Value.SetInt32(AKey);
  FCustomerDeleteCommand.Execute(ARequestFilter);
  Result := FCustomerDeleteCommand.Parameters[1].Value.GetBoolean;
end;

constructor TServerMethods1Client.Create(ARestConnection: TDSRestConnection);
begin
  inherited Create(ARestConnection);
end;

constructor TServerMethods1Client.Create(ARestConnection: TDSRestConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ARestConnection, AInstanceOwner);
end;

destructor TServerMethods1Client.Destroy;
begin
  FEchoStringCommand.DisposeOf;
  FReverseStringCommand.DisposeOf;
  FCustomerListCommand.DisposeOf;
  FCustomerListCommand_Cache.DisposeOf;
  FCustomerListIdCommand.DisposeOf;
  FCustomerListIdCommand_Cache.DisposeOf;
  FCustomerInsertCommand.DisposeOf;
  FCustomerUpdateCommand.DisposeOf;
  FCustomerDeleteCommand.DisposeOf;
  inherited;
end;

end.

