unit Unit1;

interface

uses
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Classes,
   System.Variants,
   FMX.Types,
   FMX.Controls,
   FMX.Forms,
   FMX.Graphics,
   FMX.Dialogs,
   FMX.StdCtrls,
   FMX.Controls.Presentation,
   Data.FireDACJSONConsts,
   Data.FireDACJSONReflect,
   FMX.ListView.Types,
   FMX.ListView.Appearances,
   FMX.ListView.Adapters.Base,
   FMX.ListView,
   FMX.TabControl,
   FMX.Layouts,
   FMX.Edit;

type
   TForm1 = class(TForm)
      ToolBar1: TToolBar;
      Label1: TLabel;
      Button1: TButton;
      ListView1: TListView;
      TabControl1: TTabControl;
      TabItem1: TTabItem;
      TabItem2: TTabItem;
      Button2: TButton;
      Layout1: TLayout;
      Label2: TLabel;
      Edit1: TEdit;
      Layout2: TLayout;
      Label3: TLabel;
      Edit2: TEdit;
      GridLayout1: TGridLayout;
      Button3: TButton;
      Button4: TButton;
      Button5: TButton;
      procedure Button1Click(Sender: TObject);
      procedure FormCreate(Sender: TObject);
      procedure ListView1ItemClick(const Sender: TObject; const AItem: TListViewItem);
      procedure Button3Click(Sender: TObject);
      procedure Button4Click(Sender: TObject);
      procedure Button5Click(Sender: TObject);
      procedure Button2Click(Sender: TObject);
      procedure FormShow(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form1: TForm1;

implementation

uses
   ClientModuleUnit1;

{$R *.fmx}

procedure TForm1.FormCreate(Sender: TObject);
begin
   TabControl1.TabPosition := TTabPosition.None;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
   Button1Click(Self);
end;

procedure TForm1.ListView1ItemClick(const Sender: TObject; const AItem: TListViewItem);
begin
   if ClientModule1.FDMemTable1.Locate( //
      ClientModule1.FDMemTable1.Fields[0].FieldName //
      , AItem.Tag, []) then
   begin
      Edit1.Tag := AItem.Tag;
      Edit1.Text := AItem.Tag.ToString;
      Edit2.Text := AItem.Text;

      TabControl1.ActiveTab := TabItem2;
      Button2.Visible := True;
   end;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
   dsList: TFDJSONDataSets;
   LItem: TListViewItem;
begin
   TabControl1.ActiveTab := TabItem1;
   Button2.Visible := False;

   // Change Host and Port
   // ClientModule1.DSRestConnection1.Host := '192.168.0.21';
   // ClientModule1.DSRestConnection1.Port := 8080;

   {$REGION 'Request Method'}
   dsList := TFDJSONDataSets.Create;
   dsList := ClientModule1.ServerMethods1Client.CustomerList();
   {$ENDREGION}
   {$REGION 'Data read of Server'}
   ClientModule1.FDMemTable1.Close;

   ClientModule1.FDMemTable1.AppendData( //
      TFDJsonDataSetsReader.GetListValue(dsList, 0));

   ClientModule1.FDMemTable1.Open;
   {$ENDREGION}

   if not ClientModule1.FDMemTable1.IsEmpty then
   begin
      ListView1.Items.Clear;

      ClientModule1.FDMemTable1.First;

      while not ClientModule1.FDMemTable1.Eof do
      begin
         LItem := ListView1.Items.Add;

         LItem.Tag := ClientModule1.FDMemTable1.Fields[0].AsInteger;
         LItem.Text := ClientModule1.FDMemTable1.Fields[1].AsString;
         LItem.Detail := 'Customer Id: ' + ClientModule1.FDMemTable1.Fields[0].AsString;

         ClientModule1.FDMemTable1.Next;
      end;
   end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
   Button1Click(Self);
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
   if ClientModule1.ServerMethods1Client.CustomerInsert( //
      StrToInt(Edit1.Text), Edit2.Text) then
   begin
      Button1Click(Self);
      ShowMessage('Insert OK!');
   end;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
   if ClientModule1.ServerMethods1Client.CustomerUpdate( //
      StrToInt(Edit1.Text), Edit2.Text, Edit1.Tag) then
   begin
      Button1Click(Self);
      ShowMessage('Update OK!');
   end;
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
   if ClientModule1.ServerMethods1Client.CustomerDelete( //
      StrToInt(Edit1.Text)) then
   begin
      Button1Click(Self);
      ShowMessage('Delete OK!');
   end;
end;

end.
