unit ServerMethodsUnit1;

interface

uses
   System.SysUtils,
   System.Classes,
   System.Json,
   DataSnap.DSProviderDataModuleAdapter,
   DataSnap.DSServer,
   DataSnap.DSAuth,
   FireDAC.Stan.Intf,
   FireDAC.Stan.Option,
   FireDAC.Stan.Param,
   FireDAC.Stan.Error,
   FireDAC.DatS,
   FireDAC.Phys.Intf,
   FireDAC.DApt.Intf,
   FireDAC.Stan.Async,
   FireDAC.DApt,
   FireDAC.Stan.StorageBin,
   FireDAC.Stan.StorageXML,
   FireDAC.Stan.StorageJSON,
   Data.DB,
   FireDAC.Comp.DataSet,
   FireDAC.Comp.Client,
   Data.FireDACJSONReflect,
   Data.FireDACJSONConsts,
   FMX.Dialogs;

type
   TServerMethods1 = class(TDSServerModule)
      FDStanStorageJSONLink1: TFDStanStorageJSONLink;
      FDStanStorageXMLLink1: TFDStanStorageXMLLink;
      FDStanStorageBinLink1: TFDStanStorageBinLink;
   private
      { Private declarations }
   public
      { Public declarations }
      function EchoString(Value: string): string;
      function ReverseString(Value: string): string;

      function CustomerList: TFDJSONDataSets;
      function CustomerListId(AId: Integer): TFDJSONDataSets;

      function CustomerInsert(AId: Integer; ACustomer: string): Boolean;
      function CustomerUpdate(AId: Integer; ACustomer: string; AKey: Integer): Boolean;
      function CustomerDelete(AKey: Integer): Boolean;

   end;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}
{$R *.dfm}

uses
   System.StrUtils,
   DataModuleUnit1,
   FormUnit1;

function TServerMethods1.EchoString(Value: string): string;
begin
   Result := Value;
end;

function TServerMethods1.ReverseString(Value: string): string;
begin
   Result := System.StrUtils.ReverseString(Value);
end;

function TServerMethods1.CustomerList: TFDJSONDataSets;
begin
   DM.FDQryCustomer.Close;
   DM.FDQryCustomer.SQL.Text := //
      'SELECT * FROM CUSTOMER ORDER BY CUSTOMER';

   Result := TFDJSONDataSets.Create;
   TFDJSONDataSetsWriter.ListAdd(Result, DM.FDQryCustomer);

   // Open Link - http://localhost:8080/datasnap/rest/TServerMethods1/CustomerList
end;

function TServerMethods1.CustomerListId(AId: Integer): TFDJSONDataSets;
begin
   DM.FDQryCustomer.Close;
   DM.FDQryCustomer.SQL.Text := //
      'SELECT * FROM CUSTOMER WHERE CUST_NO = ' + AId.ToString;

   Result := TFDJSONDataSets.Create;
   TFDJSONDataSetsWriter.ListAdd(Result, DM.FDQryCustomer);

   // Open Link - http://localhost:8080/datasnap/rest/TServerMethods1/CustomerListId/1001
end;

function TServerMethods1.CustomerInsert(AId: Integer; ACustomer: string): Boolean;
var
   LSQL: string;
begin
   Result := False;

   LSQL := //
      'INSERT INTO CUSTOMER (CUST_NO, CUSTOMER' + //
      ') VALUES (' + AId.ToString + ', ' + QuotedStr(ACustomer) + ')';

   try
      DM.FDQryCustomer.ExecSQL(LSQL);
      Result := True;
   except
      on E: Exception do
      begin
         Form1.Memo1.Lines.Add( //
            E.ClassName + sLineBreak + //
            E.Message + sLineBreak + //
            'SQL: ' + LSQL);
      end;
   end;

   // Open Link - http://localhost:8080/datasnap/rest/TServerMethods1/CustomerInsert/1016/Willian%20Santos
   // Open Link - http://localhost:8080/datasnap/rest/TServerMethods1/CustomerInsert/1017/Micheli%20Marcelino
end;

function TServerMethods1.CustomerUpdate(AId: Integer; ACustomer: string; AKey: Integer): Boolean;
var
   LSQL: string;
begin
   Result := False;

   LSQL := //
      'UPDATE CUSTOMER SET' + //
      ' CUST_NO = ' + AId.ToString + //
      ', CUSTOMER = ' + QuotedStr(ACustomer) + //
      ' WHERE CUST_NO = ' + AKey.ToString;

   try
      DM.FDQryCustomer.ExecSQL(LSQL);
      Result := True;
   except
      on E: Exception do
      begin
         Form1.Memo1.Lines.Add( //
            E.ClassName + sLineBreak + //
            E.Message + sLineBreak + //
            'SQL: ' + LSQL);
      end;
   end;

   // Open Link - http://localhost:8080/datasnap/rest/TServerMethods1/CustomerUpdate/1016/Willian%20Oliveira%20Santos/1016
end;

function TServerMethods1.CustomerDelete(AKey: Integer): Boolean;
var
   LSQL: string;
begin
   Result := False;

   LSQL := //
      'DELETE FROM CUSTOMER' + //
      ' WHERE CUST_NO = ' + AKey.ToString;

   try
      DM.FDQryCustomer.ExecSQL(LSQL);
      Result := True;
   except
      on E: Exception do
      begin
         Form1.Memo1.Lines.Add( //
            E.ClassName + sLineBreak + //
            E.Message + sLineBreak + //
            'SQL: ' + LSQL);
      end;
   end;

   // Open Link - http://localhost:8080/datasnap/rest/TServerMethods1/CustomerDelete/1017
end;

end.
