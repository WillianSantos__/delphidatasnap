program AppServer;

{$APPTYPE GUI}
{$R *.dres}

uses
  FMX.Forms,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  ServerMethodsUnit1 in 'Modules\ServerMethodsUnit1.pas' {ServerMethods1: TDSServerModule},
  ServerContainerUnit1 in 'Modules\ServerContainerUnit1.pas' {ServerContainer1: TDataModule},
  WebModuleUnit1 in 'Modules\WebModuleUnit1.pas' {WebModule1: TWebModule},
  FormUnit1 in 'Sources\FormUnit1.pas' {Form1},
  DataModuleUnit1 in 'Modules\DataModuleUnit1.pas' {DataModule1: TDataModule};

{$R *.res}

begin
   if WebRequestHandler <> nil then
      WebRequestHandler.WebModuleClass := WebModuleClass;

   Application.Initialize;
   Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TDataModule1, DM);
  Application.Run;

end.
